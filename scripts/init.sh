#!/bin/bash
set -e
cd $(dirname "$0")/..

source ./config

echo "# init event repository"
mkdir repo
cd repo 
git clone https://gitlab.com/trichter/leipzig.git master


echo "========================"
echo "# init builder"
cd ..
git clone https://gitlab.com/trichter/builder.git
cd builder
yarn

echo "========================"
echo "# init scraper"
cd ..
git clone https://gitlab.com/trichter/scraper.git
cd scraper
yarn