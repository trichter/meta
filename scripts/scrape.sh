#!/bin/bash
cd $(dirname "$0")/..

source ./config

echo "pull updates"
cd repo/master
git pull origin master

echo "scrape!"
cd ../../scraper
yarn scrape ../repo $1