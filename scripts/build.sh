#!/bin/bash
cd $(dirname "$0")/..

source ./config

cd builder
yarn generate ../repo

echo ""
echo "html generated in ./builder/output"
echo "use ./server.sh and browse to http://localhost:8000"

# upload example
# rsync -avz --checksum --ignore-times --delete-after ./builder/output/ user@hostname:/var/www/trichter/html
