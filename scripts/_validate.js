const TrichterDB = require('../builder/node_modules/@trichter/db').default
const path = require('path')
const db = new TrichterDB(path.join(__dirname, '../repo'))

async function main() {
    const master = await db.checkoutBranch('master')
    try {
        await master.loadData()
        console.log('everything seems fine!')
        process.exit(0)
    } catch(err) {
        console.error(err)
        process.exit(1)
    }
}

main()